# simple_project_with_docker

This is a straightforward example that demonstrates how Docker can be used for Python projects.

Here is the Medium article for this project: [Dockerizing Your First Python Project: A Beginner’s Guide](https://medium.com/@fariba.hashemi87/dockerizing-your-first-python-project-a-beginners-guide-8988be4e67e1)