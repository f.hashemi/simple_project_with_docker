"""
The project configuration module to provide all the info to the other parts.
"""
from __future__ import annotations

import os
from typing import Optional, cast

from dynaconf import LazySettings


class Config:
    """
    The singleton Config class to retrieve different configurations and parameters set in the settings.toml file.
    """

    _instance: Optional[Config] = None
    _settings: LazySettings


    def __new__(cls) -> Config:
        if cls._instance is None:
            cls._instance = super(Config, cls).__new__(cls)
            cls._settings = LazySettings(
                environments=True,
                load_dotenv=True,
                SETTINGS_FILE_FOR_DYNACONF=os.path.join(os.path.dirname(os.path.realpath(__file__)), "settings.toml"),
            )
        return cls._instance

    def env_name(self) -> str:
        """Returns environment from the settings.toml file"""
        return cast(str, self._settings["ENVIRONMENT_NAME"])

