from project_configuration import Config
if __name__ == '__main__':
    env_name = Config().env_name()
    print(f'A simple example of using docker. The current environment is {env_name}')
