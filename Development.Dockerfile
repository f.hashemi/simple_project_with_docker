FROM python:3.8-slim-buster

COPY . .

RUN pip install -r requirements.txt

ENV ENV_FOR_DYNACONF=development

CMD ["python","main.py"]